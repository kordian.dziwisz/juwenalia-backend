# juwenalia backend

Serwer API **[Słupskich juwenaliów](http://juwenalia.slupsk.pl/)**

Aby uruchomić serwer 

- uzupełnij plik `config.ts` odpowiednimi informacjami o serwerze
- zaimportuj domyślną bazę  danych `juwenalia.sql`
- zbuduj aplikację za pomocą `npm run build`
- uruchom serwer za pomocą `npm run start`

Jeśli jesteś developerem przeczytaj **[Wiki](https://gitlab.com/kordian.dziwisz/juwenalia-backend/-/wikis/home)**