-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 19, 2020 at 09:23 AM
-- Server version: 10.4.12-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `juwenalia`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organizers` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `imagePath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `organizers`, `place`, `date`, `dateEnd`, `meta`, `createdAt`, `updatedAt`, `imagePath`) VALUES
(1, 'Koncert', 'Taki tam koncert', 'Akademia Pomorska', 'Główna Sala', '2020-02-13 00:00:00', '2020-02-14 00:00:00', '0', '2020-02-11 00:00:00', '2020-02-11 00:00:00', 'events/1.png'),
(2, 'Muzyka Folkloru', 'Taki sobie koncert', 'Percival', 'Scena', '2020-02-13 08:00:00', '2020-02-13 10:00:00', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00', 'events/2.jpg'),
(3, 'Muzyka Rock', 'Taki sobie lepszy koncert', 'The Beatles', 'Scena', '2020-02-13 10:00:00', '2020-02-13 12:00:00', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00', 'events/3.png'),
(4, 'Muzyka Pop', 'Koncert po przerwie', 'Birdy', 'Mała Scena', '2020-02-13 14:00:00', '2020-02-13 15:00:00', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00', 'events/3.png'),
(5, 'Muzyka Poważna', 'Ostatni Koncert', 'Słupska Symfonietta', 'Scena', '2020-02-13 18:00:00', '2020-02-13 20:00:00', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `imagePath` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `date`, `meta`, `createdAt`, `updatedAt`, `imagePath`) VALUES
(7, 'edycja 3', '<p class=\"ql-align-center\"><strong class=\"ql-size-huge\" style=\"color: rgb(230, 0, 0);\"><em><u>Curae</u></em></strong><strong style=\"color: rgb(230, 0, 0);\"><em><u>,</u></em></strong> Vivamus lacus pede, molestie vitae, vulputate accumsan.<span style=\"background-color: rgb(153, 51, 255); color: rgb(255, 255, 255);\"> Quisque</span><sup> sed</sup> est a velit cursus lectus, luctus et lectus. Nam dictum wisi vel lectus. <span class=\"ql-font-monospace\">Nullam rutrum consectetuer adipiscing elit.</span> Aenean tincidunt vel, ornare nulla eu libero. In neque vel odio quis elit. Cum <s>sociis natoque</s> penatibus et purus.</p>', '2020-02-13 16:00:00', NULL, '2020-02-14 20:09:02', '2020-02-14 20:09:02', 'news\\3.png');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `description`, `logo`, `link`, `category`, `meta`, `createdAt`, `updatedAt`) VALUES
(1, 'title1', 'desc1', 'sponsors/1.png', 'https://www.facebook.com/', 'category1', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00'),
(2, 'title2', 'desc2', 'sponsors/2.jpg', 'https://www.facebook.com/', 'category2', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00'),
(3, 'title3', 'desc3', 'sponsors/3.png', 'https://www.facebook.com/', 'category3', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00'),
(4, 'title4', 'desc4', 'sponsors/4.jpg', 'https://www.facebook.com/', 'category4', '{}', '2020-02-12 00:00:00', '2020-02-12 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
