const path = require('path')
const nodeExternals = require('webpack-node-externals')
module.exports = {
  entry: path.join(__dirname, 'src', 'main.ts'),
  target: 'node',
  devtool: 'inline-source-map',
  output: { path: path.join(__dirname, 'dist'), filename: 'bundle.js' },
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    alias: {
      '@': path.join(__dirname, 'src'),
      '@c': path.join(__dirname, 'src', 'controllers'),
      '@r': path.join(__dirname, 'src', 'routes'),
      '@m': path.join(__dirname, 'src', 'models'),
      config: path.join(__dirname, 'src', 'config'),
    },
    extensions: ['.tsx', '.ts', '.js'],
  },
}
