import { Options } from 'sequelize'
import path from 'path'

export default {
  ip: '127.0.0.2',
  port: 80,
  allowOrigin: '*',
  imagePath: path.join(__dirname, 'images'),
  db: {
    database: 'juwenalia',
    username: 'root',
    password: '',
    options: {
      host: 'localhost',
      dialect: 'mysql',
    } as Options,
  },
}
