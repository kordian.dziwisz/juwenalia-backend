export default function properties(Events: any, News: any, Sponsors: any) {
  var config = [
    {
      resource: Events,
      options: {
        parent: 'Bazy Danych',
        name: 'Wydarzenia',
        listProperties: [
          'id',
          'title',
          'description',
          'place',
          'date',
          'dateEnd',
          'imagePath',
          'organizers',
        ],
        editProperties: [
          'title',
          'description',
          'place',
          'date',
          'dateEnd',
          'imagePath',
          'organizers',
        ],
        filterProperties: [
          'id',
          'title',
          'description',
          'place',
          'date',
          'dateEnd',
          'organizers',
        ],
        properties: {
          id: { label: 'L.p.' },
          title: { label: 'Tytuł' },
          description: { type: 'richtext', label: 'Treść' },
          place: { label: ' Miejsce' },
          date: { label: 'Data Rozpoczęcia' },
          dateEnd: { label: 'Data Zakończenia' },
          imagePath: { label: 'Ścieżka zdjęcia' },
          organizers: { label: 'Organizatorzy' },
        },
      },
    },
    {
      resource: News,
      options: {
        parent: 'Bazy Danych',
        name: 'Nowości',
        listProperties: ['id', 'title', 'description', 'date', 'imagePath'],
        editProperties: ['title', 'description', 'date', 'imagePath'],
        filterProperties: ['id', 'title', 'description', 'date'],
        properties: {
          id: { label: 'L.p.' },
          title: { label: 'Tytuł' },
          description: { type: 'richtext', label: 'Treść' },
          date: { label: 'Data Publikacji' },
          imagePath: { label: 'Ścieżka zdjęcia' },
        },
      },
    },
    {
      resource: Sponsors,
      options: {
        parent: 'Bazy Danych',
        name: 'Sponsorzy',
        listProperties: ['id', 'name', 'description', 'logo', 'category'],
        editProperties: ['name', 'description', 'logo', 'link', 'category'],
        filterProperties: ['id', 'name', 'description', 'logo', 'category'],
        properties: {
          id: { label: 'L.p.' },
          name: { label: 'Nazwa' },
          description: { type: 'richtext', label: 'Opis' },
          logo: { label: 'Grafika' },
          category: { label: 'Kategoria' },
        },
      },
    },
  ]
  return config
}
