import { sponsorsController, sponsorsHeadController } from '@c/sponsors'
import { Router } from 'express'

const router = Router()

router.get('/sponsors/u', sponsorsHeadController)

router.get('/sponsors', sponsorsController)

export default router
