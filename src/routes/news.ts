import {
  newController,
  newsController,
  newHeadController,
  newsHeadController,
} from '@c/news'
import { Router } from 'express'

const router = Router()

router.get('/news/u', newsHeadController)
router.get('/news/u/:id', newHeadController)

router.get('/news', newsController)
router.get('/news/:id', newController)

export default router
