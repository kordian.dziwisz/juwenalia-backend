import AdminBro from 'admin-bro'
const AdminBroExpress = require('admin-bro-expressjs')
const AdminBroSequelize = require('admin-bro-sequelizejs')

AdminBro.registerAdapter(AdminBroSequelize)

import bazaDanych from '../utils/database'
import Events from '@m/events'
import News from '@m/news'
import Sponsors from '@m/sponsors'
import properties from '../utils/adminPanelView'

const adminBro = new AdminBro({
  databases: [bazaDanych],
  resources: properties(Events, News, Sponsors),
  branding: {
    companyName: 'Juwenalia',
    softwareBrothers: false,
  },
})

const router = AdminBroExpress.buildRouter(adminBro)

export default router
