import { Router } from 'express'
import { imageController } from '@c/image'

const router = Router()

router.get('/image', imageController)

export default router
