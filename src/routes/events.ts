import { Router } from 'express'
import {
  eventsController,
  eventController,
  eventHeadController,
  eventsHeadController,
} from '@/controllers/events'

const router = Router()

router.get('/events/u', eventsHeadController)
router.get('/events/u/:id', eventHeadController)

router.get('/events', eventsController)
router.get('/events/:id', eventController)

export default router
