import { INTEGER, STRING, DATE, TEXT, Model } from 'sequelize'
import sequelize from '@/utils/database'

class News extends Model {
  public readonly id!: number
  public title!: string
  public description: string
  public date!: Date
  public meta: JSON
  public imagePath: string

  public readonly createdAt!: Date
  public readonly updatedAt!: Date
}

News.init(
  {
    id: {
      type: INTEGER.UNSIGNED,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },

    title: {
      type: STRING,
      allowNull: false,
    },

    description: {
      type: TEXT,
      allowNull: true,
    },

    date: {
      type: DATE,
      allowNull: false,
    },

    imagePath: {
      type: STRING,
      allowNull: true,
    },

    meta: {
      type: TEXT,
      allowNull: true,
    },
  },
  { sequelize: sequelize, tableName: 'news' },
)
export default News
