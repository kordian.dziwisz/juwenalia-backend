import { INTEGER, STRING, TEXT, DATE, Model } from 'sequelize'
import sequelize from '@/utils/database'

class Events extends Model {
  public readonly id!: number
  public title!: string
  public description: string
  public organizers!: string
  public place!: string
  public date!: Date
  public dateEnd: Date
  public meta: JSON
  public imagePath: string

  public readonly createdAt!: Date
  public readonly updatedAt!: Date
}

Events.init(
  {
    id: {
      type: INTEGER.UNSIGNED,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },

    title: {
      type: STRING,
      allowNull: false,
    },

    description: {
      type: TEXT,
      allowNull: true,
    },

    organizers: {
      type: STRING,
      allowNull: false,
    },

    place: {
      type: TEXT,
      allowNull: false,
    },

    date: {
      type: DATE,
      allowNull: false,
    },

    dateEnd: {
      type: DATE,
      allowNull: true,
    },

    imagePath: {
      type: STRING,
      allowNull: true,
    },

    meta: {
      type: TEXT,
      allowNull: true,
    },
  },
  { sequelize: sequelize, tableName: 'events' },
)

export default Events
