import { INTEGER, STRING, TEXT, Model } from 'sequelize'
import sequelize from '@/utils/database'

class Sponsors extends Model {
  public readonly id!: number
  public name!: string
  public description: string
  public logo!: string
  public link: string
  public category!: string
  public meta: JSON

  public readonly createdAt!: Date
  public readonly updatedAt!: Date
}

Sponsors.init(
  {
    id: {
      type: INTEGER.UNSIGNED,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },

    name: {
      type: STRING,
      allowNull: false,
    },

    description: {
      type: TEXT,
      allowNull: true,
    },

    logo: {
      type: STRING,
      allowNull: false,
    },

    link: {
      type: STRING,
      allowNull: true,
    },

    category: {
      type: STRING,
      allowNull: false,
    },

    meta: {
      type: TEXT,
      allowNull: true,
    },
  },
  { sequelize: sequelize, tableName: 'sponsors' },
)

export default Sponsors
