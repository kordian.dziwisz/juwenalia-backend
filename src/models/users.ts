import { INTEGER, STRING, TEXT, Model, ENUM } from 'sequelize'
import sequelize from '@/utils/database'

class Users extends Model {
  public readonly id!: number
  public username!: string
  public password!: string
  public role!: string

  public readonly createdAt!: Date
  public readonly updatedAt!: Date
}

Users.init(
  {
    id: {
      type: INTEGER.UNSIGNED,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },

    username: {
      type: STRING,
      allowNull: false,
    },

    password: {
      type: STRING,
      allowNull: false,
    },

    role: {
      type: ENUM,
      values: ['admin', 'restricted'],
      allowNull: false,
    },
  },
  { sequelize: sequelize, tableName: 'users' },
)

export default Sponsors
