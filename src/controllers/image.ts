import config from '@/config'
import { Request, Response } from 'express'

export function imageController(req: Request, res: Response) {
  res.sendFile(`${config.imagePath}/${req.query.path}`)
}
