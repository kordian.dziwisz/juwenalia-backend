import config from '@/config'
import { Request, Response } from 'express'
import Events from '@/models/events'

export function eventsController(req: Request, res: Response) {
  if (Object.entries(req.query).length) {
    Events.findAll({ where: req.query }).then((data: Events[]) => {
      res.json(data)
    })
  } else {
    Events.findAll().then((data: Events[]) => {
      res.json(data)
    })
  }
}

export function eventsHeadController(req: Request, res: Response) {
  if (Object.entries(req.query).length) {
    Events.max('updatedAt', { where: req.query }).then((data: Date) => {
      res.json(data)
    })
  } else {
    Events.max('updatedAt').then((data: Date) => {
      res.json(data)
    })
  }
}

export function eventController(req: Request, res: Response) {
  Events.findByPk(req.params.id).then((data: Events) => {
    res.json(data)
  })
}

export function eventHeadController(req: Request, res: Response) {
  Events.findByPk(req.params.id).then((data: Events) => {
    res.json(data)
  })
}
