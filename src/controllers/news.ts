import { Request, Response } from 'express'
import News from '@m/news'

export function newsController(req: Request, res: Response) {
  News.findAll().then((data: News[]) => {
    res.json(data)
  })
}

export function newsHeadController(req: Request, res: Response) {
  News.max('updatedAt').then((data: Date) => {
    res.json(data)
  })
}

export function newController(req: Request, res: Response) {
  News.findByPk(req.params.id).then((data: News) => {
    res.json(data)
  })
}

export function newHeadController(req: Request, res: Response) {
  News.findByPk(req.params.id).then((data: News) => {
    res.json(data)
  })
}
