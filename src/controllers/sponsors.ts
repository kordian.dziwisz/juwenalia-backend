import { Request, Response } from 'express'
import Sponsors from '@m/sponsors'

export function sponsorsController(req: Request, res: Response) {
  Sponsors.findAll().then((data: Sponsors[]) => {
    res.json(data)
  })
}

export function sponsorsHeadController(req: Request, res: Response) {
  Sponsors.max('updatedAt').then((data: Date) => {
    res.json(data)
  })
}
