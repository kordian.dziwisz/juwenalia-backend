import express, { NextFunction, Request, Response } from 'express'
import sequelize from '@/utils/database'
import config from '@/config'

import eventsRouter from '@r/events'
import newsRouter from '@r/news'
import sponsorsRouter from '@r/sponsors'
import imageRouter from '@r/image'
import adminRouter from '@r/admin'
sequelize.sync()
const app = express()
app.use('/', (req: Request, res: Response, next: NextFunction) => {
  res.setHeader('Access-Control-Allow-Origin', config.allowOrigin)
  next()
})
app.use('/admin', adminRouter)
app.use(eventsRouter)
app.use(newsRouter)
app.use(sponsorsRouter)
app.use(imageRouter)

sequelize.sync().then((result: any) => {
  app.listen(config.port, config.ip)
})
